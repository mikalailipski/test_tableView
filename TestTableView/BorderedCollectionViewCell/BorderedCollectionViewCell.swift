//
//  BorderedCollectionViewCell.swift
//  TestTableView
//
//  Created by Nikolai Lipski on 12.03.24.
//

import UIKit

final class BorderedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.cornerRadius = 15
        backView.layer.borderWidth = 2
        backView.backgroundColor = .green
        backView.layer.borderColor = UIColor.gray.cgColor
        updateContent()
    }
    
    @IBAction func touchedDown(_ sender: Any) {
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.backView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
    }
    
    @IBAction func touchedUp(_ sender: Any) {
        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.backView.transform = .identity
        }
    }
    
    func updateContent() {
        numberLabel.text = "\(Int.random(in: 0...100))"
    }
}

