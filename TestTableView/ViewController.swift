//
//  ViewController.swift
//  TestTableView
//
//  Created by Nikolai Lipski on 12.03.24.
//

import UIKit

final class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var numberOfRows: Int = (101...400).randomElement() ?? 101
    
    lazy var timerDisplayLink: CADisplayLink = {
        let displayLink = CADisplayLink(target: self, selector: #selector(onTimerTick))
        
        displayLink.preferredFramesPerSecond = 1
        displayLink.isPaused = true
        displayLink.add(to: .main, forMode: .common)
        
        return displayLink
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        timerDisplayLink.isPaused = false
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
    }
    
    @objc
    private func onTimerTick() {
        guard let randomVisibleRow = tableView.visibleCells.randomElement(),
              let cell = randomVisibleRow as? TableViewCell,
              let randomVisibleCell = cell.collectionView.visibleCells.randomElement() as? BorderedCollectionViewCell
        else { return }
        
        randomVisibleCell.updateContent()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else {
            return UITableViewCell()
        }
        
//        cell.collectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        300
//    }
}
