//
//  TableViewCell.swift
//  TestTableView
//
//  Created by Nikolai Lipski on 12.03.24.
//

import UIKit

final class TableViewCell: UITableViewCell {
    private struct Defaults {
        static let collectionViewSideInset: CGFloat = 15
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var numberOfItems: Int = (11...40).randomElement() ?? 11
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let itemSize: CGSize = CGSize(width: 90, height: 90)
        layout.estimatedItemSize = .zero
        layout.itemSize = itemSize
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        layout.minimumInteritemSpacing = 5.0
        layout.minimumLineSpacing = 5.0
        collectionView.collectionViewLayout = layout
        
        collectionView.register(UINib(nibName: "BorderedCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "BorderedCollectionViewCell")
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension TableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BorderedCollectionViewCell", for: indexPath) as? BorderedCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        return cell
    }
}
